package com.learn.springboot.taskmanager.controllers;


import com.learn.springboot.taskmanager.dto.CreateTaskDto;
import com.learn.springboot.taskmanager.dto.ErrorResponseDto;
import com.learn.springboot.taskmanager.dto.TaskResponseDto;
import com.learn.springboot.taskmanager.dto.UpdateTaskDto;
import com.learn.springboot.taskmanager.entities.NoteEntity;
import com.learn.springboot.taskmanager.entities.TaskEntity;
import com.learn.springboot.taskmanager.services.NoteService;
import com.learn.springboot.taskmanager.services.TaskService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private NoteService noteService;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/listAll")
    public ResponseEntity<List<TaskEntity>> getTasks() {
        List<TaskEntity> tasksResponseBody = taskService.getTasks();
        return ResponseEntity.ok(tasksResponseBody);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskResponseDto> getTaskById(@PathVariable("id") Integer id) {
        TaskEntity taskEntity = this.taskService.getTaskById(id);
        List<NoteEntity> notes = this.noteService.getNotesForTask(id);

        if (taskEntity == null) {
            return ResponseEntity.notFound().build();
        }

        var taskResponse = modelMapper.map(taskEntity, TaskResponseDto.class);
        taskResponse.setNotes(notes);

        return ResponseEntity.ok(taskResponse);
    }

    @PostMapping("")
    public ResponseEntity<TaskEntity> addTask(@RequestBody CreateTaskDto taskDto) throws ParseException {
        TaskEntity task = this.taskService.addTasks(taskDto.getTitle(), taskDto.getDescription(), taskDto.getDeadline());
        return ResponseEntity.ok(task);
    }

    @ExceptionHandler(ParseException.class)
    public ResponseEntity<ErrorResponseDto> handleExceptions(Exception e) {

        if (e instanceof ParseException)
            return ResponseEntity.badRequest().body(new ErrorResponseDto("Invalid date format"));

        return ResponseEntity.internalServerError().body(new ErrorResponseDto("Internal server error"));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<TaskEntity> updateTask(@PathVariable("id") int id, @RequestBody UpdateTaskDto taskDto)
            throws ParseException {

        TaskEntity taskEntity = this.taskService.updateTask(
                id, taskDto.getTitle(),
                taskDto.getDescription(),
                taskDto.getDeadline(),
                taskDto.getStatus()
        );

        if (taskEntity == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(taskEntity);
    }
}
