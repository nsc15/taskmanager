package com.learn.springboot.taskmanager.controllers;

import com.learn.springboot.taskmanager.dto.CreateNoteDto;
import com.learn.springboot.taskmanager.dto.CreateNoteResponseDto;
import com.learn.springboot.taskmanager.entities.NoteEntity;
import com.learn.springboot.taskmanager.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks/{taskId}/notes")
public class NotesController {

    @Autowired
    private NoteService noteService;

    @GetMapping("")
    public ResponseEntity<List<NoteEntity>> getNotes(@PathVariable("taskId") Integer id) {
        var notes = this.noteService.getNotesForTask(id); // 11 or 17 feature
        if (notes == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(notes);
    }

    @PostMapping()
    public ResponseEntity<CreateNoteResponseDto> addNotes(
            @PathVariable("taskId") Integer taskId,
            @RequestBody CreateNoteDto noteDto) {
        var notes = this.noteService.addNotesToTask(taskId, noteDto.getTitle(), noteDto.getBody());

        return ResponseEntity.ok(new CreateNoteResponseDto(taskId, notes));
    }
}
