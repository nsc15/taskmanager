package com.learn.springboot.taskmanager.services;

import com.learn.springboot.taskmanager.entities.NoteEntity;
import com.learn.springboot.taskmanager.entities.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class NoteService {
    @Autowired
    private TaskService taskService;

    private HashMap<Integer, TaskNotesHandler> notesHandlerHashMap = new HashMap<>();

    class TaskNotesHandler {
        protected int noteId = 1;
        protected ArrayList<NoteEntity> notes = new ArrayList<>();
    }

    public NoteEntity addNotesToTask(Integer taskId, String title, String body) {

        TaskEntity taskEntity = this.taskService.getTaskById(taskId);

        if (taskEntity == null) {
            return null;
        }

        if (notesHandlerHashMap.get(taskId) == null) {
            notesHandlerHashMap.put(taskId, new TaskNotesHandler());
        }

        TaskNotesHandler taskNotesHandler = notesHandlerHashMap.get(taskId);
        NoteEntity noteEntity = new NoteEntity();
        noteEntity.setId(taskNotesHandler.noteId);
        noteEntity.setTitle(title);
        noteEntity.setBody(body);
        taskNotesHandler.notes.add(noteEntity);
        taskNotesHandler.noteId++;

        return noteEntity;
    }

    public ArrayList<NoteEntity> getNotesForTask(Integer taskId) {
        TaskEntity task = this.taskService.getTaskById(taskId);
        if (task == null) return null;

        if (notesHandlerHashMap.get(taskId) == null) {
            notesHandlerHashMap.put(taskId, new TaskNotesHandler());
        }
        return notesHandlerHashMap.get(taskId).notes;
    }
}
