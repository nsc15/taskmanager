package com.learn.springboot.taskmanager.services;

import com.learn.springboot.taskmanager.entities.TaskEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Service
public class TaskService {
    private int taskId = 1;
    private SimpleDateFormat deadlineFormat = new SimpleDateFormat("yyyy-MM-dd");
    private ArrayList<TaskEntity> taskEntities = new ArrayList<>();

    // add tasks to the array list
    public TaskEntity addTasks(String title, String description, String deadline) throws ParseException {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(taskId);
        taskEntity.setTitle(title);
        taskEntity.setDescription(description);
        taskEntity.setDeadline(deadlineFormat.parse(deadline)); // TODO: Need to validate the date format & parse string to date
        taskEntity.setStatus(false);
        taskEntities.add(taskEntity);
        taskId++; // increment the taskId for every task added
        return taskEntity;
    }

    /**
     * fetch all the tasks in an ArrayList
     *
     * @return ArrayList<TaskEntity>
     */
    public ArrayList<TaskEntity> getTasks() {
        return taskEntities;
    }

    /**
     * find and get task by taskId
     *
     * @param id: int
     * @return: TaskEntity
     */
    public TaskEntity getTaskById(Integer id) {
        System.out.println("List of tasks: " + taskEntities);
        return taskEntities.stream().findAny().filter(e -> e.getId() == id).orElse(null);
    }

    public TaskEntity updateTask(int id, String title, String description, String deadline, Boolean status) throws RuntimeException, ParseException {
        TaskEntity task = getTaskById(id);
        if (task == null)
            return null;

        if (title != null)
            task.setTitle(title);

        if (description != null)
            task.setDescription(description);

        if (deadline != null)
            task.setDeadline(deadlineFormat.parse(deadline));

        if (status != null)
            task.setStatus(status);
        return task;
    }


}
