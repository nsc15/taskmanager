package com.learn.springboot.taskmanager.dto;

import com.learn.springboot.taskmanager.entities.NoteEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CreateNoteResponseDto {
    private Integer taskId;
    private NoteEntity note;
}
