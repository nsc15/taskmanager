package com.learn.springboot.taskmanager.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UpdateTaskDto {
    String title;
    String description;
    String deadline;
    Boolean status;
}
