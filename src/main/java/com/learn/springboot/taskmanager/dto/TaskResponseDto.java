package com.learn.springboot.taskmanager.dto;

import com.learn.springboot.taskmanager.entities.NoteEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class TaskResponseDto {
    private int id;
    private String title;
    private String description;
    private Date deadline;
    private Boolean status;
    private List<NoteEntity> notes;
}
